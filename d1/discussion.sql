-- MySQL Advances Queries and Joins

-- [SECTION] Add new records
-- Add 5 artists, atleast 2 albums each, 1-2 songs per album.

-- Add 5 artists
	-- Taylor Swift
	-- Lady Gaga
	-- Justine Bieber
	-- Ariana Grande
	-- Bruno Mars

INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justine Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Insert the following records in their respective tables
    -- Artist: Taylor Swift
        -- Album: Fearless 2008-11-11 
            -- Songs:
                -- Fearless, 402, "Pop Rock" 
                -- Love Story, 355, "Country Pop"
        -- Album: Red, 2012-10-22 
            -- Songs:
                -- State of Grace, 455, "Rock, Alternative Rock, Arena Rock"
                -- Red, 341, "Country"

    -- Artist: Lady Gaga
        -- Album: A Star Is Born, 2018-10-05 
            -- Songs:
                -- Black Eyes, 304, "Rock and Roll" 
                -- Shallow, 336, "Country, Rock, Folk Rock"
        -- Album: Born This Way, 2011-05-23 
            -- Song: Born This Way, 420, "Electropop"

    -- Artist: Justin Bieber
        -- Album: Purpose, 2015-11-13 
            -- Song: Sorry, 320, "Dancehall-poptropical Housemoombahton"
        -- Album: Believe, 2012-06-15 
            -- Song: Boyfriend, 252, "Pop"

    -- Artist: Ariana Grande
        -- Album: Dangerous Woman, 2016-05-20 
            -- Song: Into You, 405, "EDM House"
        -- Album: Thank U, Next, 2019-02-08  
            -- Song: Thank U, Next, 327, "Pop, R&B"

    -- Artist: Bruno Mars
        -- Album: 24k Magic, 2016-11-18 
            -- Song: 24k Magic, 346, "Funk, Disco, R&B"
        -- ALbum: Earth to Mars,2011-02-07 
            -- Song: Lost, 321, "Pop"

-- Taylor Swift Albums and Songs
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 402, "Pop Rock", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", 355, "Country Pop", 3);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-10-22", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", 455, "Rock, Alternative Rock, Arena Rock", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", 341, "Country", 4);

-- Lady Gaga Albums and Songs
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-10-05", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 304, "Rock and roll", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 336, "Country, Rock, Folk Rock", 5);	

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-05-23", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 420, "Electropop", 6);

-- Justin Bieber Albums and Songs

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-11-13", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 320, "Dancehall-poptropical Housemoombahton", 7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012-06-15", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 252, "Pop", 8);

-- Ariana Grande

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-05-20", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 405, "EDM House", 9);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-02-08", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", 327, "Pop, R&B", 10);

-- Bruno Mars

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016-11-18", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", 346, "Funk, Disco, R&B", 11);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-02-07", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 321, "Pop", 12);

-- [SECTION] Advanced Selects

-- Exclude records
SELECT * FROM songs WHERE id != 11;

-- LIMIT: Show only specific number of records.
SELECT * FROM songs LIMIT 5;

-- Greater than or equal/Less than or equal (Comparison)
SELECT * FROM songs WHERE id <= 11;
SELECT * FROM songs WHERE id >= 11;

-- Get specific records using OR
SELECT * FROM songs WHERE id = 1 OR id = 5 OR id = 9;

-- Get specific records using "IN"
-- a shorthand method for multiple OR conditions.
SELECT * FROM songs WHERE id IN (1, 5, 9);

SELECT * FROM songs WHERE genre IN ("Pop", "K-Pop");

-- Combining conditions

SELECT * FROM songs WHERE album_id = 4 AND id < 8;

-- Find Partial matches.
-- LIKE is used in a WHERE clause to search for a specified pattern in a column.
-- There are two wildcards used in conjunction with LIKE.
    -- (%) - Represents zero, one or multiple characters.
    -- (_) - Represents a single character. (2012-__-__)

SELECT * FROM songs WHERE song_name LIKE "%e"; -- select keywords from the end.

SELECT * FROM songs WHERE song_name LIKE "a%"; -- select keyword from the start.

SELECT * FROM songs WHERE song_name LIKE "%a%"; -- select keyword ANYWHERE.

SELECT * FROM albums WHERE date_released LIKE "____-__-__";

SELECT * FROM albums WHERE date_released LIKE "201_-0_-0_"; -- get a record based on a specific pattern.

-- Sorting records (alphanumeric order: A-Z/Z-A)
-- Syntax: SELECT * FROM table_name ORDER BY column_name ASC/DESC;
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- Getting distinct records.
-- Eliminates duplicate rows and display a unique list of values.
SELECT DISTINCT genre FROM songs;

-- [SECTION] Table Joins
-- This is used to retrieve data from multiple tables.
-- This is perforemed whenever two or more tables are listed in a SQL statement.
-- Visual Joins: https://joins.spathon.com/

-- Syntax of Inner Join (Join)
/*
    Two Tables:

    SELECT * (or column_name) FROM table1_name
        JOIN table2_name ON table1_column_id_pk = table2_column_id_fk;

    Multiple Tables:
    SELECT * (or column_name) FROM table1_name
        JOIN table2_name ON table1_column_id_pk = table2_column_id_fk
        JOIN table3_name ON table2_column_id_pk = table3_column_id_fk;

*/

-- Combine artists and albums table.
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id;

-- Combine more than two tables.
-- ARTISTS creates ALBUMS contains SONGS
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id;

-- Specify columns to be included in the Join table.
SELECT artists.name, albums.album_title, songs.song_name FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id;

-- Insert another artist
INSERT INTO artists (name) VALUE ("Ed Sheeran");

-- Show artists without records on the right side of the joined table.
SELECT artists.name, albums.album_title FROM artists
    LEFT JOIN albums ON artists.id = albums.artist_id;

-- Right JOIN
SELECT artists.name, albums.album_title FROM artists
    RIGHT JOIN albums ON artists.id = albums.artist_id;

-- FULL OUTER JOIN
    -- UNION: is used to combine the result set of two or more SELECT statements;

SELECT artists.name, albums.album_title FROM artists
    LEFT JOIN albums ON artists.id = albums.artist_id
    UNION
SELECT artists.name, albums.album_title FROM artists
    RIGHT JOIN albums ON artists.id = albums.artist_id;   
